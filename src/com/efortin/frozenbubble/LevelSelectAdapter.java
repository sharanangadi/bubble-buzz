package com.efortin.frozenbubble;

import java.util.List;

import com.mass.bubblebuzz.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class LevelSelectAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final List<String> names;

    static class ViewHolder {
        public TextView text;
    }

    public LevelSelectAdapter(Activity context, List<String> names) {
        super(context, R.layout.level_select_item, names);
        this.context = context;
        this.names = names;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        // reuse views
        if (rowView == null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.level_select_item, null);
            // configure view holder
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.text = (TextView) rowView.findViewById(R.id.textViewLevel);
            rowView.setTag(viewHolder);
        }

        // fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.text.setText(names.get(position));

        return rowView;
    }
}