/**
 * 
 */
package com.efortin.frozenbubble;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.efortin.frozenbubble.util.GoogleAnalyics;
import com.mass.bubblebuzz.FrozenBubble;
import com.mass.bubblebuzz.R;

/**
 * @author sharanu
 * 
 */
public class LevelSelectActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.grid_layout);
        GridView gridView = (GridView) findViewById(R.id.grid_view);
        List<String> levels = new ArrayList<String>();
        int level = PreferenceManager.getDefaultSharedPreferences(this.getApplicationContext()).getInt("currentLevel", 1);
        for (int i = 0; i < level; i++) {
            levels.add((i + 1) + "");
        }
        // Instance of ImageAdapter Class
        gridView.setAdapter(new LevelSelectAdapter(this, levels));

        gridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                GoogleAnalyics.addView(LevelSelectActivity.this.getApplicationContext(), "LevelSelectActivity:" + position);
                Intent intent = new Intent(LevelSelectActivity.this, FrozenBubble.class);
                intent.putExtra("myPlayerId", (int) FrozenBubble.myPlayerId);
                intent.putExtra("numPlayers", (int) 1);
                intent.putExtra("gameLocale", (int) FrozenBubble.LOCALE_LOCAL);
                intent.putExtra("gameLevel", position);
                intent.putExtra("fromMassLevel", true);

                startActivity(intent);
                /*
                 * Terminate the splash screen activity.
                 */
                finish();
            }
        });
    }
    
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
      if (keyCode == KeyEvent.KEYCODE_BACK) {
        /*
         * Create an intent to launch the home screen.
         */
        Intent intent = new Intent(this, HomeScreen.class);
        intent.putExtra("startHomeScreen", true);
        startActivity(intent);
        finish();
        return true;
      }
      return super.onKeyDown(keyCode, event);
    }
}
