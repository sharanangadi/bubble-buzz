/**
 * 
 */
package com.efortin.frozenbubble.util;

import android.app.Activity;
import android.content.Context;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.analytics.tracking.android.GoogleAnalytics;
import com.google.analytics.tracking.android.Tracker;
import com.mass.bubblebuzz.R;

/**
 * @author sharanu
 * 
 */
public class GoogleAnalyics {

	private static final String TAG = "ET:GoogleAnalyics";
	private static Tracker mGaTracker;
	private static GoogleAnalytics mGaInstance;

	public static void addView(Context context, String view) {
		 try {
			// Get the GoogleAnalytics singleton. Note that the SDK uses
			// the application context to avoid leaking the current context.
			mGaInstance = GoogleAnalytics.getInstance(context);
			// Use the GoogleAnalytics singleton to get a Tracker.
			mGaTracker = mGaInstance.getTracker(context.getResources().getString(R.string.ga_trackingId)); // Placeholder tracking ID.
			
			// Send a screen view when the Activity is displayed to the user.
			mGaTracker.sendView(view);
		} catch (Exception e) {
			MyLog.d(TAG, "Can't add track " + view + " error : " + e.getMessage());
		}
	}
	
	public static void addView(Context context, String view, String trackId) {
		 try {
			// Get the GoogleAnalytics singleton. Note that the SDK uses
			// the application context to avoid leaking the current context.
			mGaInstance = GoogleAnalytics.getInstance(context);
			// Use the GoogleAnalytics singleton to get a Tracker.
			mGaTracker = mGaInstance.getTracker(trackId); // Placeholder tracking ID.
			
			// Send a screen view when the Activity is displayed to the user.
			mGaTracker.sendView(view);
		} catch (Exception e) {
			MyLog.d(TAG, "Can't add track " + view + " error : " + e.getMessage());
		}
	}

	public static void startActivityAnalytics(Activity context) {
		EasyTracker.getInstance().activityStart(context); // Add this method.
	}
	
	public static void stopActivityAnalytics(Activity context) {
		EasyTracker.getInstance().activityStop(context); // Add this method.
	}
	
}
