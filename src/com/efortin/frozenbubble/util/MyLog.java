/**
 * 
 */
package com.efortin.frozenbubble.util;

import android.util.Log;

/**
 * @author sharanu
 * 
 */
public class MyLog {

	public static void d(final String TAG, final String msg) {
		Log.d(TAG, msg);
	}

	public static void d(final String TAG, final String msg,
			final Throwable throwable) {
		Log.d(TAG, msg, throwable);
	}
	
	public static void v(final String TAG, final String msg) {
		Log.v(TAG, msg);
	}

	public static void v(final String TAG, final String msg,
			final Throwable throwable) {
		Log.v(TAG, msg, throwable);
	}

	public static void i(final String TAG, final String msg) {
		Log.i(TAG, msg);
	}

	public static void i(final String TAG, final String msg,
			final Throwable throwable) {
		Log.i(TAG, msg, throwable);
	}

	public static void e(final String TAG, final String msg) {
		Log.e(TAG, msg);
	}

	public static void e(final String TAG, final String msg,
			final Throwable throwable) {
		Log.e(TAG, msg, throwable);
	}
}
