/**
 * 
 */
package com.efortin.frozenbubble.util;

/**
 * @author sharanu
 *
 */
public class Constants {
    public static String AD_PUBLISHERS_CODE = "a15369c737c64d4";
    public static String FULLAD_PUBLISHERS_CODE = "a15369c76ad96b9";
    public static String STORE_URL = "https://play.google.com/store/apps/details?id=com.mass.bubblebuzz";
}
